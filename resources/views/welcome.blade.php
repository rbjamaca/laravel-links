@extends('layouts.app')
@section('content')
            <table class="table table-striped table-bordered">
                    <thead>
                        <th class="text-center" width='15%'>TITLE</th>
                        <th class="text-center">URL</th>
                        <th class="text-center" width='10%'>ACTION</th>
                    </thead>
                    <tbody>
                    @foreach ($links as $link)
                        <tr>
                            <td>{{ $link->title }}</td>
                            <td><a href="{{ $link->url }}"> {{ $link->description }} </a></td>
                            <td>
                            <div class="btn-group">
                            <form action="{{ url('/update/'.$link->id) }}" method="POST">
                                @method('GET')
                                <input type="submit" value="UPDATE" class="btn btn-primary">
                            </form>
                            &nbsp;
                            <form action="{{url("/", $link->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="DELETE" class="btn btn-danger">
                            </form>
                            </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
                {{-- <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div> --}}
            </div>
        </div>
@endsection
