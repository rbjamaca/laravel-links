@extends('layouts.app');
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="row">
            <form action="{{url("/update", $id)}}" method="POST">
                    @csrf
                    @method('PUT')

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            Please fix the following errors
                        </div>
                    @endif
                        <div class="form-group">
                            <label for="title">TITLE</label>
                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title')}}">
                            @error('title')
                                <div class="invalid-feedback"> {{ $message}} </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="url">URL</label>
                            <input type="text" name="url" class="form-control @error('url') is-invalid @enderror" value="{{ old('url')}}">
                            @error('url')
                                <div class="invalid-feedback"> {{ $message}} </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">DESCRIPTION</label>
                            <input type="text" name="description" class="form-control @error('desciption') is-invalid @enderror" value="{{ old('description')}}">
                            @error('description')
                                <div class="invalid-feedback"> {{ $message}} </div>
                            @enderror
                        </div>
                    <button class="btn btn-primary" type="submit">submit</button>

                </form>
            </div>
        </div>
    </div>
@endsection