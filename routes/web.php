<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Link;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $links = \App\Link::all();

    return view('welcome', ['links' => $links]);
});

Route::get('/submit', function () {
    return view('submit');
});

Route::post('/submit', function (Request $request) {
    $data = $request->validate([
        'title' => 'required|max:255',
        'url' => 'required|url|max:255',
        'description' => 'required|max:255',
    ]);
    

    $link = Link::create($data);
    // $link = tap(new App\Link($data))->save();

    return redirect('/');
});

Route::get('/update/{id}', function ($id) {
    return view('update', ['id' => $id]);
});

Route::put('update/{id}', function (Request $request, $id) {

    $data = $request->validate([
        'title' => 'required|max:255',
        'url' => 'required|url|max:255',
        'description' => 'required|max:255',
    ]);
    
    $link = Link::where('id', $id)->update($data);

    return redirect('/');

});

Route::delete('/{id}', function ($id) {
    $link = Link::find($id);
    if($link){
        $destroy = Link::destroy($id);
    }

    return redirect('/');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
